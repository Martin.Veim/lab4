package datastructure;

import cellular.CellState;

public class CellGrid implements IGrid {

    private int rows;
    private int cols;
    private CellState[][] cells;

    public CellGrid(int rows, int columns, CellState initialState) {
        this.rows = rows;
        this.cols = columns;
        this.cells = new CellState[rows][columns];
        for (int col = 0; col < cols; col++) {
            for (int row = 0; row < rows; row++) {
                this.cells[row][col] = initialState;
            }
        }


	}

    @Override
    public int numRows() {
        return this.rows;
    }

    @Override
    public int numColumns() {
        return this.cols;
    }

    @Override
    public void set(int row, int column, CellState element) {
        this.cells[row][column] = element;
    }

    @Override
    public CellState get(int row, int column) {
        return this.cells[row][column];
    }

    @Override
    public IGrid copy() {
        CellGrid localCopy = new CellGrid(this.rows, this.cols, null);
        for (int col = 0; col < cols; col++) {
            for (int row = 0; row < rows; row++) {
                localCopy.set(row, col, this.get(row,col));
            }
        }
        return localCopy;
    }
    
}
